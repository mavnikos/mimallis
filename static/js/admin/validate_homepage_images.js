$(document).ready(function () {

    let singleImageSelectors = [$('#id_image')];

    if (typeof(singleImageSelectors) != "undefined") {
        let singleImageRules = {
            'w': {
                'opr': 'lte',
                'value': 1920,
            },
            'h': {
                'opr': 'lte',
                'value': 1080,
            },
        };
        // Validate single images (that do not possess any `add-row` button)
        singleImageSelectors.forEach(function (el) {
            el.prop('accept', 'image/*');
            el.change(function () {
                djangoAdminObj.validateImage(el, singleImageRules);
            });
        });
    }
});
