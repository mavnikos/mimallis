$(window).on("load", function () {

    const group = $('#images-group');

    if (typeof(group) !== "undefined") {
        let imageRules = {
            'w': {
                'opr': 'lte',
                'value': 1920,
            },
            'h': {
                'opr': 'lte',
                'value': 1080,
            },
        };

        // Validate the initial multiple images (at first there is at least one - depends on `extra` attribute inside the admin class)
        let multiImageSelectors = $.map(group.find('input[type=file]'), function(el) { return $(el); });
        multiImageSelectors.forEach(function (el) {
            el.change(function () {
                djangoAdminObj.validateImage(el, imageRules);
            });
        });

        // Validate each additional image that was added via the corresponding button with class .add-row
        console.log(group);
        console.log(group.find('.add-row a'));
        group.find('.add-row a').on("click", function () {
            // We want only the row , the user just added. So, go get it
            //var extra = group.find('.last-related input[type=file]').slice(-2);
            let extra = $('.add-row').prev().prev().find('input[type=file]');
            console.log(extra);
            console.log(extra.parent());
            // When a new row is added, remove any previous styling, since this is a brand new element!
            djangoAdminObj.removePreviousWarnings(extra.parent(), extra);
            extra.on("change", function () {
                console.log('changed!');
                djangoAdminObj.validateImage($(this), imageRules);
            });
        });
    }
});
