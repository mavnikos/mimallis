window.URL = window.URL || window.webkitURL;

let jQuery = $ = django.jQuery;  // use Django's jQuery version (as of Django 1.9 jQuery == 2.1.4)


let djangoAdminObj = (function() {
    "use strict";

    // Validation parameters
    let allowedFileTypes = ['jpg', 'jpeg', 'JPG', 'JPEG', 'png'];

    return {
        // Returns a string in the format: (<file_type>)|(<file_type>)|(<file_type>) from `allowedFileTypes` let
        regexFmt: function regexFmt() {
            let s = '';
            allowedFileTypes.forEach(function (el) {
                s += '(' + el + ')|';
            });
            return s.slice(0, -1);
        },

        // Given the parent element and the <input>, it removes any warnings (from previous validations)
        removePreviousWarnings: function removePreviousWarnings(parentDiv, inputObj) {
            inputObj.css('border', 'none');
            parentDiv.removeClass('errors');
            parentDiv.find('.errorlist').remove();
        },

        // Given the parent element of the <input>, the <input> element and the message, it adds error validation styling
        addWarnings: function addWarnings(parentDiv, inputObj, msg) {
            inputObj.css('border', '2px solid #ba2121');
            parentDiv.addClass('errors');
            parentDiv.prepend(msg);
        },

        // Assumes that there is a placeholder to the left of the <input> element. This func adds the to-be-uploaded image
        previewImage: function previewImage(fileInput, parentInput) {
            let reader = new FileReader();
            reader.onload = function (e) {
                // get the element where inside it, the image will be shown
                // care must be taken because Django has 2 way of showing inline elements (Tabular -- uses <table> and Stacked -- uses <div>)
                let tabularInline = parentInput.prev('td').find('p');
                let stackedInline = parentInput.prev('div').find('p');
                let pElem = stackedInline.length === 1 ? stackedInline : tabularInline;
                // clean its interior
                pElem.find('span').remove();
                pElem.find('img').remove();
                // append the new image
                pElem.append('<img src="' + e.target.result + '" alt="" width="70px" height="90px">');
            };
            reader.readAsDataURL(fileInput.files[0]);
        },

        // Given the image width & height and the rule to be applied, this func performs the image validation calculations
        checkRules: function checkRules(img_width, img_height, rule) {
            let validAxis = ['w', 'h'];
            let validOperator = ['eq', 'gt', 'gte', 'lt', 'lte'];
            let valid = false;
            let msg = [];  // holder to insert potential validation error messages
            // `w` and `h` args are the dimensions of the image being loaded.
            // `rule` arg format: {
            //                         'w'|'h': { 'opr': 'eg'|'gt'|...|'lte',
            //                                    'value': <integer>
            //                                  },
            //                             ... another one for the other axis
            //                     }

            // The length of `rule` obj must be between [1, 2]
            if (Object.keys(rule).length > 0 && Object.keys(rule).length <= 2) {
                for (let axis in rule) {
                    // `axis` must be a property of `rule`
                    if (rule.hasOwnProperty(axis)) {
                        // `axis` must be of length 1 AND be one of the valid axis values
                        if ((axis.length === 1) && (validAxis.indexOf(axis) > -1)) {
                            // `opr` must be one of the valid operators AND `value` must be an integer
                            if ((validOperator.indexOf(rule[axis]['opr']) > -1) && (typeof rule[axis]['value'] === 'number' && rule[axis]['value'] % 1 === 0)) {
                                let axisChecked = axis === validAxis[0] ? [img_width, 'πλάτος'] : [img_height, 'ύψος'];
                                let opr = rule[axis]['opr'];
                                let value = rule[axis]['value'];
                                switch (opr) {
                                    case 'eq':
                                        if (!(axisChecked[0] === value)) {
                                            msg.push('<li>Το ' + axisChecked[1] + ' της φωτογραφίας που έβαλες δεν είναι σωστό!<br>' +
                                            'Θα πρέπει να είναι ακριβώς: ' + value + 'px. Εσύ έβαλες <strong>' + axisChecked[0] + 'px</strong></li>')
                                        }
                                        break;
                                    case 'gt':
                                        if (!(axisChecked[0] > value)) {
                                            msg.push('<li>Το ' + axisChecked[1] + ' της φωτογραφίας που έβαλες δεν είναι σωστό!<br>' +
                                            'Θα πρέπει να είναι μεγαλύτερο από: ' + value + 'px. Εσύ έβαλες <strong>' + axisChecked[0] + 'px</strong></li>')
                                        }
                                        break;
                                    case 'gte':
                                        if (!(axisChecked[0] >= value)) {
                                            msg.push('<li>Το ' + axisChecked[1] + ' της φωτογραφίας που έβαλες δεν είναι σωστό!<br>' +
                                            'Θα πρέπει να είναι μεγαλύτερο ή ίσο από: ' + value + 'px. Εσύ έβαλες <strong>' + axisChecked[0] + 'px</strong></li>')
                                        }
                                        break;
                                    case 'lt':
                                        if (!(axisChecked[0] < value)) {
                                            msg.push('<li>Το ' + axisChecked[1] + ' της φωτογραφίας που έβαλες δεν είναι σωστό!<br>' +
                                            'Θα πρέπει να είναι μικρότερο από: ' + value + 'px. Εσύ έβαλες <strong>' + axisChecked[0] + 'px</strong></li>')
                                        }
                                        break;
                                    case 'lte':
                                        if (!(axisChecked[0] <= value)) {
                                            msg.push('<li>Το ' + axisChecked[1] + ' της φωτογραφίας που έβαλες δεν είναι σωστό!<br>' +
                                            'Θα πρέπει να είναι μικρότερο ή ίσο από: ' + value + 'px. Εσύ έβαλες <strong>' + axisChecked[0] + 'px</strong></li>')
                                        }
                                        break;
                                }
                            } else {
                                console.log('`opr` must be one of the valid operators AND `value` must be an integer. opr = ' + rule[axis]['opr'] + ' --- value = ' + rule[axis]['value']);
                            }
                        } else {
                            console.log('`axis` must be of length 1 AND be one of the valid axis values. axis = ' + axis + ' --- validAxis = ' + validAxis);
                        }
                    } else {
                        console.log('`axis` must be a property of `rule`');
                    }
                }
            }
            return msg;
        },

        // Given an image and the rules (rules may be empty), this func validates the image
        validateImage: function validateImage(image, rules) {
            let self = this;
            let msg = '';
            let fileInputObj = image;  //fileInputObj == Object of <input> with other properties as well
            let parentDiv = fileInputObj.closest('td').length === 1 ? fileInputObj.closest('td') : fileInputObj.closest('div');
            let fileInput = fileInputObj[0];  //fileInput == Just the Object of <input>
            let file = fileInput.files && fileInput.files[0];  // instance of File object of undefined

            if (file) {
                // 1. Check file format
                let proceed = false;
                let fileType = fileInput.files[0].type;
                let regex = new RegExp('^image\/\.' + this.regexFmt(allowedFileTypes) + '$');
                if (!regex.test(fileType)) { // if image format fails, warn the user
                    self.removePreviousWarnings(parentDiv, fileInputObj);
                    msg = '<ul class="errorlist">' +
                    '<li>Ο τύπος του αρχείου που έβαλες δεν είναι σωστός!<br>Το αρχείο θα πρέπει να έχει μια από τις ακόλουθες καταλήξεις: <strong> ' + allowedFileTypes.join(', ') + '</strong>.<br>' +
                    'Εσύ έβαλες: <strong>' + fileType.split('/')[1] + '</strong></li></ul>';
                    self.addWarnings(parentDiv, fileInputObj, msg);
                    proceed = false;
                } else {  // if image format passes, remove any previous potential warnings
                    proceed = true;
                    self.removePreviousWarnings(parentDiv, fileInputObj);
                }

                // 2. Check image dimensions only if the above test has passed
                if (proceed) {
                    let img = new Image();
                    img.src = window.URL.createObjectURL(file);

                    img.onload = function () {
                        let imgWidth = img.naturalWidth,
                            imgHeight = img.naturalHeight;
                        window.URL.revokeObjectURL(img.src);
                        // if `rules` arg is passed then call `checkRules`, else just skip validation and just preview the image (always [])
                        let errorMessages = typeof(rules) != "undefined" ? self.checkRules(imgWidth, imgHeight, rules) : [];
                        if (errorMessages.length === 0) { // `errorMessages`  === []. No errors. Image is valid!
                            self.removePreviousWarnings(parentDiv, fileInputObj);
                            fileInputObj.css('border', '2px solid green');
                            self.previewImage(fileInput, parentDiv);
                        }
                        else { // `errorMessages` is not empty. Show error messages!
                            // First remove any previous ones
                            self.removePreviousWarnings(parentDiv, fileInputObj);
                            msg = '<ul class="errorlist">' + errorMessages.join(' ') + '</ul>';
                            self.addWarnings(parentDiv, fileInputObj, msg);
                            self.previewImage(fileInput, parentDiv);
                        }
                    };
                }
            }
        }
    };

})();
