/**
 * jQuery.browser.mobile (http://detectmobilebrowser.com/)
 * jQuery.browser.mobile will be true if the browser is a mobile device
 *
 **/
(function(a){(jQuery.browser=jQuery.browser||{}).responsive=/(ipad|android|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);


const responsiveDisplay = jQuery.browser.responsive;

function reloads(){
    let body = jQuery('body');
    let position = jQuery(document).scrollTop();
    jQuery(window).ready(function(){
        if (position > jQuery('.header-top').outerHeight()) {
            jQuery('body').addClass('fixing').css({'padding-top' : '0'});
        }else{
			body.removeClass('fixing');
    		body.css({ 'padding-top': '0'});
		}
    });
}

function StickyHeader(){
	let body = jQuery('body');
	let HtopHeight = jQuery('.header-top').outerHeight();
	let bodyFix = body.hasClass('fixHeader');
	if (body.hasClass('fixHeader')) {

    	jQuery('.main-header').each(function() {
    		let IdHeader = this.id;

	    	jQuery(document).on("scroll", function(){
	    		let scrollTop = parseInt(jQuery(document).scrollTop(), 10);
				if(scrollTop > HtopHeight){
					body.addClass('fixing');
	        		if (IdHeader === 'header-style2' && bodyFix) {
	        			jQuery('body').css({'padding-top': '50px'});
	        		}
	        		else if (IdHeader === 'header-style3' && bodyFix) {
	        			jQuery('body').css({'padding-top': '100px'});
	        		}
	        		else if (IdHeader === 'header-default') {
	        			jQuery('body').css({'padding-top': '70px'});
	        		}

				}else{
					body.removeClass('fixing');
	        		body.css({ 'padding-top': '0px'});
				}
	    	});
	    });
    }
}


// Body overlay responsive actions
function Overlay(){
    jQuery('body').on('click','.responsive-overlay',function(){
        jQuery('nav.main-nav').animate({width:'toggle'},350);
        jQuery('body').css({'overflow':'visible'});
        jQuery('.responsive-overlay').remove();
    });
}

// Search Toggle Nav
// function SearchToggle(){
//     jQuery('.search-icon').on('click', function() {
//         jQuery(this).toggleClass('active');
//         jQuery('.search-box-wrapper').slideToggle();
//         if (jQuery(this).hasClass('active')) {
//             jQuery('.search-box-wrapper').find('input').focus();
//         }
//     });
// }

// Main Menu Superfish
function MainMenu(){
	let vpToggle = 979;
	let w = window.innerWidth;
	let menuClone = jQuery('nav.main-nav'),
    	divMenu   = '<div class="menu-mobile"></div>';
    	menuClone.clone();

    /* Header style 2 */
	if (responsiveDisplay || w < vpToggle) {
		jQuery('#menu_toggle_button').show();
		let MenuMobDisplay = jQuery('#menu_toggle_button').css('display');
		/* if show button menu, its mean responsive , clone main nav*/
		if ( MenuMobDisplay !== 'none' ) {
			jQuery('body').prepend(divMenu);
            menuClone.appendTo('.menu-mobile');
        	jQuery('#main_menu').superfish('destroy');
            jQuery('.header-menu nav.main-nav').addClass('hidden');
		} else {
			jQuery('.menu-mobile').remove();
        	menuClone.appendTo('.header-menu .container');
            jQuery('.header-menu nav.main-nav').removeClass('hidden');
            jQuery('.header-menu nav.main-nav').show();
            jQuery('#main_menu').superfish('init');
		}
	} else {
		jQuery('#menu_toggle_button').hide();
	}

	jQuery("#main_header nav ul li ul").parent("li").addClass("sub-menu");
    jQuery("#main_header nav ul li div").parent("li").addClass("sub-menu");
    if( w > vpToggle && !jQuery('#main_menu').hasClass('sf-js-enabled') ) {
        jQuery('#main_menu').superfish({
            popUpSelector: 'ul',
            delay:          300,
            animation:      {opacity:'show'},
            animationOut:   {opacity:'hide'},
            speed:          'fast',
            speedOut:       'fast',
            cssArrows:      true,
            disableHI:      true
        });
    }

    jQuery(window).on("resize", function() {
		let w 		  = window.innerWidth;
		let menuClone = jQuery('nav.main-nav'),
	    	divMenu   = '<div class="menu-mobile"></div>';
	    	menuClone.clone();
    	let flag;

        jQuery('.responsive-overlay').remove();
        jQuery('body').css({'overflow':'visible'});

        // Desktop
        if( w > vpToggle && flag !== true && !jQuery('#main_menu').hasClass('sf-js-enabled') ) {
        	flag = true;
        	jQuery('.menu-mobile').remove();
        	menuClone.appendTo('.header-menu .container');
            jQuery('.header-menu nav.main-nav').removeClass('hidden');
            jQuery('.header-menu nav.main-nav').show();
            jQuery('#main_menu').superfish('init');
            jQuery('#menu_toggle_button').hide();
        }

        // mobile
        else if( w < vpToggle && flag !== false && jQuery('#main_menu').hasClass('sf-js-enabled')) {
        	flag = false;

            jQuery('body').prepend(divMenu);
            menuClone.appendTo('.menu-mobile');
        	jQuery('#main_menu').superfish('destroy');
            jQuery('.header-menu nav.main-nav').addClass('hidden');
            jQuery('#menu_toggle_button').show();
        }
        StickyHeader();
    }).resize();
}


// Responsive Menu
function ResponsiveMenu(){
	jQuery('#menu_toggle_button').on("click", function(){
        jQuery('.main-nav').animate({width:'toggle'},350).css("display", "grid");
        if( !jQuery('.responsive-overlay').length ) {
            jQuery('body').prepend('<div class="responsive-overlay"></div>');
            jQuery('body').css({'overflow':'hidden'});
        } else {
            jQuery('.responsive-overlay').remove();
        }
    });
}


// function parallax() {
// 	jQuery('nav').localScroll(800);
//     let id_parallax = jQuery('#parallax'),
//         id_testimoni = jQuery('#testimoni'),
//         id_bannerPage = jQuery('#banner');
//
//
//     /* Main Section Parallax BG */
//     if (id_parallax) {
//         let bgParallax = jQuery('#parallax').attr('data-parallax');
//         if(responsiveDisplay){
//             jQuery('#parallax').each(function(){
//                 jQuery(this).addClass('parallax-mobile');
//                 jQuery('.parallax-mobile').css('background-image', 'url('+bgParallax+')');
//          });
//         }else{
//             jQuery('#parallax').parallax();
//             if (bgParallax) {
//                 jQuery('#parallax').css('background-image', 'url('+bgParallax+')');
//             }
//         }
//     }
//
//     /* Testimonial section Parallax BG*/
//     if (id_testimoni) {
//         var parallaxBg = jQuery('#testimoni').attr('data-parallax');
//         if(responsiveDisplay){
//             jQuery('#testimoni').each(function(){
//                 jQuery(this).addClass('parallax-mobile');
//                 jQuery('.parallax-mobile').css('background-image', 'url('+parallaxBg+')');
//          });
//         }else{
//             if (parallaxBg) {
//                 jQuery('#testimoni').css('background-image', 'url('+parallaxBg+')');
//             }
//             jQuery('#testimoni').parallax('0%', 0.01);
//         }
//     }
//
//     /* Banner page Parallax BG*/
//     if (id_bannerPage) {
//         var bannerBg = jQuery('#banner').attr('data-parallax');
//         if(responsiveDisplay){
//             jQuery('#banner').each(function(){
//                 jQuery(this).addClass('parallax-mobile');
//                 jQuery('.parallax-mobile').css('background-image', 'url('+bannerBg+')');
//          });
//         }else{
//             jQuery('#banner').parallax();
//             if (bannerBg) {
//                 jQuery('#banner').css('background-image', 'url('+bannerBg+')');
//             }
//         }
//     }
// }

// function mainCounter(jQueryobject,interval,max,increment) {
//     var number = parseInt(jQueryobject.text(),10) + increment;
//     if (number < max){
//         setTimeout(function() {mainCounter(jQueryobject,interval,max,increment);} ,interval);
//         jQueryobject.text(number);
//     }
//     else{
//         jQueryobject.text(max);
//     }
// }

// function counterIndex(){
//     if(!responsiveDisplay){
//         jQuery(".number_counter").one('inview', function(event, isInView) {
//             if (isInView) {
//                 var max = jQuery(this).data("number");
//                 var increment = 1;
//                 if (max > 50) increment = 10;
//                 if (max > 500) increment = 100;
//                 if (max > 5000) increment = 200;
//                 if (max > 10000) increment = 1000;
//                 var interval = jQuery(this).data("duration")/(max/increment);
//                 jQuery(this).text('0');
//                 mainCounter(jQuery(this),interval,max,increment);
//             }
//         });
//     }
//     else{
//         jQuery(".number_counter").each(function() {
//             var max = jQuery(this).data("number");
//             jQuery(this).text(max);
//         });
//     }
// }


// function wowo(){
//     var wow = new WOW({
//         mobile: false
//     });
//     wow.init();
// }


function teamSlide(){
    jQuery(".team_slides").owlCarousel({
        slideSpeed : 600,
        paginationSpeed: 1000,
        autoPlay: false,
        items : 4,
        itemsDesktop : [1170,4],
        itemsDesktopSmall : [960,4],
        itemsTablet: [768,2],
        itemsMobile : [480,1],
        itemsMobileSmall: [360, 1],
        navigation:true,
        pagination:false,
        navigationText : false,
        lazyLoad: true
    });
}

function testiSlide(){
    jQuery(".testimonial-slide").owlCarousel({
        slideSpeed : 600,
        paginationSpeed: 1000,
        autoPlay: false,
        items : 1,
        itemsDesktop : [1170,1],
        itemsDesktopSmall : [960,1],
        itemsTablet: [768,1],
        itemsMobile : [480,1],
        itemsMobileSmall: [360, 1],
        navigation:true,
        pagination:false,
        navigationText : false,
        lazyLoad: true
    });
}

// function clientSlide(){
//     jQuery(".clientSlides").owlCarousel({
//         slideSpeed : 600,
//         paginationSpeed: 2000,
//         autoPlay: false,
//         items : 6,
//         itemsDesktop : [1170,4],
//         itemsDesktopSmall : [960,4],
//         itemsTablet: [768,3],
//         itemsMobile : [480,2],
//         itemsMobileSmall: [360, 1],
//         navigation:true,
//         pagination:false,
//         navigationText : false
//     });
// }

// function BlogPostSlide(){
//     jQuery(".img_blog").owlCarousel({
//         slideSpeed : 800,
//         paginationSpeed: 1000,
//         autoPlay: false,
//         items : 1,
//         itemsDesktop : [1170,1],
//         itemsDesktopSmall : [960,1],
//         itemsTablet: [768,1],
//         itemsMobile : [480,1],
//         itemsMobileSmall: [360, 1],
//         navigation:false,
//         pagination:true,
//         navigationText : false
//     });
// }

// function RelatedProject(){
//     jQuery(".portfolio-related").owlCarousel({
//         slideSpeed : 800,
//         paginationSpeed: 800,
//         autoPlay: false,
//         items : 4,
//         itemsDesktop : [1170,4],
//         itemsDesktopSmall : [960,4],
//         itemsTablet: [768,3],
//         itemsMobile : [480,1],
//         itemsMobileSmall: [360, 1],
//         navigation:false,
//         pagination:true,
//         navigationText : false
//     });
// }

// function massonry_main(value1000, value800, value500, value320, idContainere, marginBottom, marginRight){
//     (function($, window, document, undefined) {
//         'use strict';
//         // console.log(value1000,value800, value500, value320, idContainere, marginBottom, marginRight);
//         let gridContainer = $(idContainere),
//             filtersContainer = $('#filters-container'),
//             wrap, filtersCallback;
//
//         /*********************************
//             init cubeportfolio
//          *********************************/
//         gridContainer.cubeportfolio({
//             layoutMode: 'grid',
//             rewindNav: true,
//             scrollByPage: false,
//             defaultFilter: '*',
//             animationType: 'quicksand',
//             gapHorizontal: marginBottom,
//             gapVertical: marginRight,
//             gridAdjustment: 'responsive',
//             mediaQueries: [{
//                 width: 1600,
//                 cols: 5
//             },{
//                 width: 1100,
//                 cols: value1000
//             }, {
//                 width: 800,
//                 cols: value800
//             }, {
//                 width: 500,
//                 cols: value500
//             }, {
//                 width: 320,
//                 cols: value320
//             }],
//             caption: 'minimal',
//             displayType: 'lazyLoading',
//             displayTypeSpeed: 100,
//
//             // lightbox
//             lightboxDelegate: '.cbp-lightbox',
//             lightboxGallery: true,
//             lightboxTitleSrc: 'data-title',
//             lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',
//
//             // singlePage popup
//             singlePageDelegate: '.cbp-singlePage',
//             singlePageDeeplinking: true,
//             singlePageStickyNavigation: true,
//             singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
//             singlePageCallback: function(url, element) {
//                 // to update singlePage content use the following method: this.updateSinglePage(yourContent)
//                 var t = this;
//
//                 $.ajax({
//                         url: url,
//                         type: 'GET',
//                         dataType: 'html',
//                         timeout: 5000
//                     })
//                     .done(function(result) {
//                         t.updateSinglePage(result);
//                     })
//                     .fail(function() {
//                         t.updateSinglePage("Error! Please refresh the page!");
//                     });
//             },
//
//             // single page inline
//             singlePageInlineDelegate: '.cbp-singlePageInline',
//             singlePageInlinePosition: 'above',
//             singlePageInlineInFocus: true,
//             singlePageInlineCallback: function(url, element) {
//                 // to update singlePage Inline content use the following method: this.updateSinglePageInline(yourContent)
//             }
//         });
//
//
//         /*********************************
//             add listener for filters
//          *********************************/
//         // if (filtersContainer.hasClass('cbp-l-filters-dropdown')) {
//         //     wrap = filtersContainer.find('.cbp-l-filters-dropdownWrap');
//         //
//         //     wrap.on({
//         //         'mouseover.cbp': function() {
//         //             wrap.addClass('cbp-l-filters-dropdownWrap-open');
//         //         },
//         //         'mouseleave.cbp': function() {
//         //             wrap.removeClass('cbp-l-filters-dropdownWrap-open');
//         //         }
//         //     });
//         //
//         //     filtersCallback = function(me) {
//         //         wrap.find('.cbp-filter-item').removeClass('cbp-filter-item-active');
//         //         wrap.find('.cbp-l-filters-dropdownHeader').text(me.text());
//         //         me.addClass('cbp-filter-item-active');
//         //         wrap.trigger('mouseleave.cbp');
//         //     };
//         // } else {
//         //     filtersCallback = function(me) {
//         //         me.addClass('cbp-filter-item-active').siblings().removeClass('cbp-filter-item-active');
//         //     };
//         // }
//         //
//         // filtersContainer.on('click.cbp', '.cbp-filter-item', function() {
//         //     var me = $(this);
//         //
//         //     if (me.hasClass('cbp-filter-item-active')) {
//         //         return;
//         //     }
//         //
//         //     // get cubeportfolio data and check if is still animating (reposition) the items.
//         //     if (!$.data(gridContainer[0], 'cubeportfolio').isAnimating) {
//         //         filtersCallback.call(null, me);
//         //     }
//         //
//         //     // filter the items
//         //     gridContainer.cubeportfolio('filter', me.data('filter'), function() {});
//         // });
//
//
//         /*********************************
//             activate counter for filters
//          *********************************/
//         // gridContainer.cubeportfolio('showCounter', filtersContainer.find('.cbp-filter-item'), function() {
//         //     // read from url and change filter active
//         //     var match = /#cbpf=(.*?)([#|?&]|$)/gi.exec(location.href),
//         //         item;
//         //     if (match !== null) {
//         //         item = filtersContainer.find('.cbp-filter-item').filter('[data-filter="' + match[1] + '"]');
//         //         if (item.length) {
//         //             filtersCallback.call(null, item);
//         //         }
//         //     }
//         // });
//
//
//         /*********************************
//             add listener for load more
//          *********************************/
//         // $('.cbp-l-loadMore-button-link').on('click.cbp', function(e) {
//         //     e.preventDefault();
//         //     var clicks, me = $(this),
//         //         oMsg;
//         //
//         //     if (me.hasClass('cbp-l-loadMore-button-stop')) {
//         //         return;
//         //     }
//         //
//         //     // get the number of times the loadMore link has been clicked
//         //     clicks = $.data(this, 'numberOfClicks');
//         //     clicks = (clicks) ? ++clicks : 1;
//         //     $.data(this, 'numberOfClicks', clicks);
//         //
//         //     // set loading status
//         //     oMsg = me.text();
//         //     me.text('LOADING...');
//         //
//         //     // perform ajax request
//         //     $.ajax({
//         //         url: me.attr('href'),
//         //         type: 'GET',
//         //         dataType: 'HTML'
//         //     }).done(function(result) {
//         //         var items, itemsNext;
//         //
//         //         // find current container
//         //         items = $(result).filter(function() {
//         //             return $(this).is('div' + '.cbp-loadMore-block' + clicks);
//         //         });
//         //
//         //         gridContainer.cubeportfolio('appendItems', items.html(),
//         //             function() {
//         //                 // put the original message back
//         //                 me.text(oMsg);
//         //
//         //                 // check if we have more works
//         //                 itemsNext = $(result).filter(function() {
//         //                     return $(this).is('div' + '.cbp-loadMore-block' + (clicks + 1));
//         //                 });
//         //
//         //                 if (itemsNext.length === 0) {
//         //                     me.text('NO MORE WORKS');
//         //                     me.addClass('cbp-l-loadMore-button-stop');
//         //                 }
//         //             });
//         //
//         //     }).fail(function() {
//         //         // error
//         //     });
//         // });
//
//     })(jQuery, window, document);
// }

// function PortfolioSetting(){
//     let idContainernya = jQuery('.cbp-l-grid-masonry').attr('id');
//     if (idContainernya ==='gallery-full') {
//         massonry_main('4','3','2','1', '#gallery-full', 10, 10);
//     }else if (idContainernya ==='gallery-2col') {
//         massonry_main('2','2','1','1', '#gallery-2col', 10, 10);
//     }else if (idContainernya ==='gallery-3col') {
//         massonry_main('3','3','1','1', '#gallery-3col', 10, 10);
//     }else if (idContainernya ==='gallery-fullscreen') {
//         massonry_main('4','3','2','1', '#gallery-fullscreen', 0, 0);
//     }else if (idContainernya ==='gallery-full-standard') {
//         massonry_main('4','3','2','1', '#gallery-full-standard', 20, 20);
//     }else if (idContainernya ==='gallery-2col-standard') {
//         massonry_main('2','2','2','1', '#gallery-2col-standard', 20, 20);
//     }else if (idContainernya ==='gallery-3col-standard') {
//         massonry_main('3','3','2','1', '#gallery-3col-standard', 20, 20);
//     }else if (idContainernya ==='grid-container') {
//         massonry_main('1','1','1','1', '#grid-container', 50, 0);
//     }else {
//         massonry_main('1','1','1','1', '#grid-container-default', 50, 0);
//     }
// }

// function videoBg(){
//     if (jQuery('#videoBg').hasClass('html5')) {
//         var mp4_file = jQuery('#videoBg').attr('data-mp4');
//         var webm_file = jQuery('#videoBg').attr('data-webm');
//
//         jQuery('#videoBg').append('<video loop autoplay muted> <source type="video/mp4" src="'+mp4_file+'"></source> <source type="video/webm" src="'+webm_file+'"></source> </video>');
//     };
//
//     if (jQuery('#videoBg').hasClass('youtube')) {
//         var youtube_id = jQuery('#videoBg').attr('youtube-id');
//         jQuery('#videoBg').YTPlayer({
//             ratio: 16 / 9, // change as needed
//             // videoId: 'BaHZ3uPvhBc',
//             videoId: youtube_id,
//             mute: true,
//             repeat: true,
//             autoPlay:true,
//             fitToBackground: true
//         });
//     };
// }


// function accrodion_toggle(){
//     var AccorLink = jQuery('h2.accordion-link');
//     AccorLink.on('click', function(e){
//         e.preventDefault();
//
//         var parentCheck = jQuery(this).parents('.accordion-wrapper'),
//             accordItems = jQuery('.accordion-wrapper'),
//             accordContent = jQuery('.accordion-content');
//
//         if( !parentCheck.hasClass('active')) {
//             accordContent.slideUp(400, function(){
//                 accordItems.removeClass('active');
//             });
//             parentCheck.find('.accordion-content').slideDown(400, function(){
//                 parentCheck.addClass('active');
//             });
//
//         } else {
//             accordContent.slideUp(400, function(){
//                 accordItems.removeClass('active');
//             });
//         }
//     });
//
//     /*Accordion Toggle*/
//     var ToggleLink = jQuery('h2.toggle-link');
//     ToggleLink.on('click', function(e){
//         e.preventDefault();
//         var parentCheck = jQuery(this).parents('.toggle-wrapper');
//
//         if( !parentCheck.hasClass('active')) {
//             parentCheck.find('.toggle-content').slideDown(400, function(){
//                 parentCheck.addClass('active');
//             });
//         } else {
//             parentCheck.find('.toggle-content').slideUp(400, function(){
//                 parentCheck.removeClass('active');
//             });
//
//         }
//     });
// }


// function pbar(){
//     if(!responsiveDisplay){
//         jQuery(".pb-value-bg .pb-value").width(0).one('inview', function(event, isInView) {
//           if (isInView) {
//             jQuery(this).animate({width: jQuery(this).data("value") + '%'}, {
//                 duration:1500,
//                 step: function(now) {
//                     jQuery(this).find('span').html(Math.floor(now) + '%');
//                     jQuery(this).find('span.main-value').animate({
//                         opacity: 1
//                     }, 1500);
//                 }
//             });
//           }
//         });
//     }
//     else{
//         jQuery(".pb-value-bg .pb-value").each(function(){
//             jQuery(this).css('width', jQuery(this).data("value")+'%');
//         });
//     }
// }


function revolutionSlider(){
    jQuery(document).ready(function() {
        jQuery('.tp-banner').show().revolution(
        {
            dottedOverlay:"none",
            delay:4000,
            startwidth:1170,
            startheight:520,
            hideThumbs:200,

            thumbWidth:100,
            thumbHeight:50,
            thumbAmount:5,

            navigationType:"bullet",
            navigationArrows:"solo",
            navigationStyle:"preview2",

            touchenabled:"on",
            onHoverStop:"on",

            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,

            parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

            keyboardNavigation:"off",

            navigationHAlign:"center",
            navigationVAlign:"bottom",
            navigationHOffset:0,
            navigationVOffset:30,

            soloArrowLeftHalign:"left",
            soloArrowLeftValign:"center",
            soloArrowLeftHOffset:20,
            soloArrowLeftVOffset:0,

            soloArrowRightHalign:"right",
            soloArrowRightValign:"center",
            soloArrowRightHOffset:20,
            soloArrowRightVOffset:0,

            shadow:0,
            fullWidth:"on",
            fullScreen:"off",

            spinner:"default",

            stopLoop:"off",
            stopAfterLoops:-1,
            stopAtSlide:-1,

            shuffle:"off",

            autoHeight:"off",
            forceFullWidth:"on",

            hideThumbsOnMobile:"off",
            hideNavDelayOnMobile:1500,
            hideBulletsOnMobile:"off",
            hideArrowsOnMobile:"off",
            hideThumbsUnderResolution:0,

            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            startWithSlide:0,
            fullScreenOffsetContainer: ".header"
        });
    }); //ready
}

function flexsliders () {
    jQuery('.flexslider').flexslider({
        // https://github.com/woocommerce/FlexSlider/wiki/FlexSlider-Properties
        animation: "fade",
        controlNav: false,
        touch: true,
        slideshow: false,
        // slideshowSpeed: 6000,
        before: function (slider) {
            //instead of going over every single slide, we will just lazy load the next one
            let slides = slider.slides,
                index = slider.animatingTo,
                $slide = jQuery(slides[index]),
                $img = $slide.find('img[data-src]');
            if ($img) {
                $img.attr("src", $img.attr('data-src')).removeAttr("data-src");
            }
        }
    });
}

function kidsAgeInput() {
    let kidsAgeDiv = jQuery(".kids__age");
    jQuery("#id_kids").on("change", function() {
        kidsAgeDiv.empty();
        let kids = parseInt(jQuery(this).val());
        for (let i=1; i <= kids; i++) {
            kidsAgeDiv.append('' +
                '<div class="kid__age__wrapper">' +
                '<label for="id_kids_age_' + i + '">Kid ' + i + ' age:</label>' +
                '<input type="tel" name="kid_age_' + i + '" id="id_kids_age_' + i + '" maxlength="2" size="2">' +
                '</div>'
            );
        }
    });
}

// function MainTabs () {
//     let tabsNav    = jQuery('.tabs-nav'),
//     tabsNavLis = tabsNav.children('li'),
//     tabContent = jQuery('.tab-content');
//
//     tabsNav.each(function($) {
//         var $this = jQuery(this);
//         $this.next().children('.tab-content').stop(true,true).hide().first().show();
//     });
//
//     tabsNavLis.on('click', function(e) {
//         var $this = jQuery(this);
//
//         $this.siblings().removeClass('active').end().addClass('active');
//
//         $this.parent().next().children('.tab-content').stop(true,true).hide().siblings( $this.find('a').attr('href') ).fadeIn();
//
//         e.preventDefault();
//     });
// }

function myDatepicker() {
    let config = {
        format: "d M yyyy",
        autoclose: true,
        startDate: new Date(),
    };
    jQuery(".datepicker").datepicker(config);

    // dp.on("changeDate", function (e) {
    //     let d = jQuery(this).datepicker('getUTCDate');
    //     e.target.value = d.toISOString();
    // });
}


function messageGenerator() {
    let d = new Date();
    let message = "";
    let hour = d.getHours();
    if (hour > 0 && hour <= 6) { message = "Please close the computer, it's too late"; }
    else if (hour >= 7 && hour <= 10) { message = "Good morning! Wishes for good weather"; }
    else if (hour >= 11 && hour <= 13) { message = "It's lunch time"; }
    else if (hour >= 14 && hour <= 17) { message = "Time for coffee break"; }
    else if (hour >= 18 && hour <= 21) { message = "Please don't tell me you're still at work. It's late"; }
    else { message = "It's a good time for a night walk"; }
    return message;
}

function messageCountry(country) {
    let msg = messageGenerator();
    if (country) {
        msg = msg + " in " + country;
    }
    return msg + "!";
}

function ipLookUp() {
    let el = jQuery("#greeting");
    let ip = el.data("clientIp");
    jQuery.ajax('https://extreme-ip-lookup.com/json/' + ip)
        .then(
            function success(response) {
                let msg = messageCountry(response.country);
                el.append(msg);
            },
            function fail() {
                let msg = messageGenerator();
                el.append(msg);
            }
        );
}


jQuery(document).ready(function($) {
    "use strict";
    // jQuery('[data-toggle="tooltip"]').tooltip();
    StickyHeader();
    MainMenu();
    Overlay();
    ResponsiveMenu();
    // SearchToggle();
    // parallax();
    // wowo();
    // counterIndex();
    teamSlide();
    testiSlide();
    // clientSlide();

    /*PORTFOLIO HERE*/
    // PortfolioSetting();

    // videoBg();
    reloads();
    // accrodion_toggle();
    // pbar();
    revolutionSlider();
    // BlogPostSlide();
    // contactForm();

    flexsliders();
    // RelatedProject();
    myDatepicker();
    // MainTabs();
    ipLookUp();
    kidsAgeInput();

    /*===========================================================*/
    /*  Preloader
    /*===========================================================*/
    //<![CDATA[
        $(window).load(function() { // makes sure the whole site is loaded
            $("#preloader").delay(350).fadeOut("slow"); // will fade out the white DIV that covers the website.
        });
    //]]>
});
