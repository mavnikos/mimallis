from django.urls import path, re_path, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin

from django.contrib.sitemaps.views import sitemap
from django.conf import settings

from homepage import views as homepage_views
from contact import views as contact_views
from .sitemap import SITEMAPS


urlpatterns = [
    # path("i18n/", include("django.conf.urls.i18n")),
    path("%s/" % settings.MY_ADMIN_URL, admin.site.urls),
    path("", homepage_views.index, name="homepage"),
    path("houses/", include("house.urls")),
    path("contact/", contact_views.index, name="contact"),
]

# urlpatterns = [path("i18n/", include("django.conf.urls.i18n"))]

# urlpatterns += i18n_patterns(
#     path("%s/" % settings.MY_ADMIN_URL, admin.site.urls),
#     path("", homepage_views.index, name="homepage"),
#     path("houses/", include("house.urls")),
#     path("contact/", contact_views.index, name="contact"),
# )

# Uncomment below to support sitemap
urlpatterns += [
   re_path(r"^sitemap\.xml/$",
           sitemap,
           {"sitemaps": SITEMAPS, "template_name": "sitemap.xml"},
           name="django.contrib.sitemaps.views.sitemap"
           )
]

# Uncomment below to serve uploaded files and custom error pages
if settings.DEBUG:
    from django.views.static import serve
    from django.views.defaults import page_not_found, server_error

    urlpatterns += [
        re_path(
            r"^media/(?P<path>.*)",
            serve,
            {"document_root": settings.MEDIA_ROOT},
        ),
        re_path(
            r"^favicon/(?P<path>.*)",
            serve,
            {"document_root": settings.ROOT("favicon")},
        ),
    ]

    urlpatterns += [
        path("404/", page_not_found, {"exception": ""}),
        path("500/", server_error),
    ]
