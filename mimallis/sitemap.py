from django.contrib import sitemaps
from django.urls import reverse

from house.models import House


class HouseSitemap(sitemaps.Sitemap):
    changefreq = "yearly"
    priority = 0.6
    # i18n = True

    def items(self):
        return House.objects.all()

    def lastmod(self, obj):
        return obj.updated_at

    def location(self, obj):
        return obj.get_absolute_url()


class StaticViewSitemap(sitemaps.Sitemap):
    changefreq = "yearly"
    priority = 0.5
    # i18n = True

    def items(self):
        return ["homepage", "contact", "house_list"]

    def location(self, obj):
        return reverse(obj)


SITEMAPS = {
    "house": HouseSitemap,
    "static": StaticViewSitemap
}

