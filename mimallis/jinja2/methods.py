"""
Custom methods for Jinja2 defined inside
['OPTIONS']['globals'] dict key setting.
"""
from django.conf import settings
from django.utils.translation import get_language_info as gli

from easy_thumbnails.templatetags import thumbnail as _thumbnail

from house.models import House


def lang_info(languages=settings.LANGUAGES):
    """
    Returns a dict of language info using the get_language_info of the
    utils.translation module.
    :param tuple languages: a tuple of tuples ('lang_code', 'lang_name')
    :return: dict
    """
    info = [gli(lang[0]) for lang in languages]
    return info


def get_houses():
    return House.objects.all()


def thumbnail(source, **kwargs):
    kwargs["crop"] = kwargs.get("crop", True)
    return _thumbnail.get_thumbnailer(source).get_thumbnail(kwargs)
