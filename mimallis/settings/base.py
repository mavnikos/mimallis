from json import loads
from os.path import abspath, dirname, join

from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _

author = "nick"


def here(*path):
    return join(abspath(dirname(__file__)), *path)


def root_is(*path):
    return join(abspath(PROJECT_ROOT), *path)


PROJECT_ROOT = here("..", "..")
ROOT = root_is
with open(ROOT("mimallis/settings/secret.json")) as f:
    secrets = loads(f.read())


def get_secret(setting, secret=secrets):
    """Get the secret variable or return explicit exception."""
    try:
        return secret[setting]
    except KeyError:
        error_msg = "Set the {0} environment variable".format(setting)
        raise ImproperlyConfigured(error_msg)


PROJECT_NAME = "mimallis"
ROOT_URLCONF = "mimallis.urls"
WSGI_APPLICATION = "mimallis.wsgi.application"
SITE_ID = 1


# APP CONFIGURATION
# used for translation of models inside the db. Must go 1st in list!
MODELTRANSLATION_APP = ["modeltranslation"]

DJANGO_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "django.contrib.flatpages",
    "django.contrib.sitemaps",
]

THIRD_PARTY_APPS = [
    "django_jinja",
    "ckeditor",
    "easy_thumbnails",
    'compressor',
]

PROJECT_APPS = [
    "dtl_utils.apps.DtlUtilsConfig",
    "homepage.apps.HomepageConfig",
    "house.apps.HouseConfig",
    "contact.apps.ContactConfig",
]

INSTALLED_APPS = (
    MODELTRANSLATION_APP + DJANGO_APPS + THIRD_PARTY_APPS + PROJECT_APPS
)


# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.BrokenLinkEmailsMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]


# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = get_secret("SECRET_KEY")

TIME_ZONE = "Europe/Athens"
LANGUAGE_CODE = "en"
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Paths which include locale directories (used for translation)
LOCALE_PATHS = (ROOT("mimallis/locale"),)

# Preferred languages of the site
LANGUAGES = (
    ("en", _("English")),
    # ("el", _("Greek")),
)


TEMPLATES = [
    {
        "BACKEND": "django_jinja.backend.Jinja2",
        "DIRS": [ROOT("templates/")],
        "APP_DIRS": True,
        "OPTIONS": {
            # render with Jinja2 all templates under DIRS that end with .html
            "match_extension": ".html",
            # do not use Jinja2 to render any admin, ckeditor or sitemap pages
            "match_regex": r"^(?!admin|registration|ckeditor|sitemap).*",
            # enable i18n with new style, which means that instead of this
            # (invalid):
            # {{ _('Hello %(w)s!')|format(w='world') }}
            # we can do this
            # (valid):
            # {{ _('Hello %(w)s!', w='world') }}
            "newstyle_gettext": True,
            "extensions": [
                "jinja2.ext.i18n",
                "django_jinja.builtins.extensions.CsrfExtension",
                "django_jinja.builtins.extensions.CacheExtension",
                "django_jinja.builtins.extensions.TimezoneExtension",
                "django_jinja.builtins.extensions.UrlsExtension",
                "django_jinja.builtins.extensions.StaticFilesExtension",
                "django_jinja.builtins.extensions.DjangoFiltersExtension",
                "compressor.contrib.jinja2ext.CompressorExtension",
            ],
            # [Available settings]
            # github.com/niwinz/django-jinja/blob/master/django_jinja/
            # backend.py#L157
            # [Usage]
            # github.com/niwinz/django-jinja/blob/master/django_jinja/builtins
            # /extensions.py#L99
            "bytecode_cache": {"enabled": True},
            # values here must be NOT callables, just a simple key:value pair.
            "constants": {
                "SITE_TITLE": _("Mimallis"),
                "EMAIL": "info@mimallis.gr",
                "TEL": "+30 22870 21094",
                "MOB": "+30 6972 808758",
                "ADDR_1": "Milos island, Cyclades",
                "ADDR_2": "Greece, 84800",
                "GM_API_KEY": get_secret("GOOGLE_MAPS_API_KEY"),
            },
            # values here MUST be 'path.to.modules' or 'path.to.functions'.
            "globals": {
                "messages": "django.contrib.messages.api.get_messages",
                "translation": "django.utils.translation",
                "settings": "django.conf.settings",
                "lang_info": "mimallis.jinja2.methods.lang_info",
                "get_houses": "mimallis.jinja2.methods.get_houses",
                "thumbnail": "mimallis.jinja2.methods.thumbnail",
            },
            # values here MUST be path to a callable (i.e function)
            "filters": {
                "remove_accent": "mimallis.jinja2.filters.remove_accent",
                "striptags": "django.template.defaultfilters.striptags",
            },
        },
    },
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "APP_DIRS": True,
        "DIRS": [ROOT("templates"), ROOT("templates/sitemap")],
        "OPTIONS": {
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.request",
            ]
        },
    },
]


MEDIA_URL = "/media/"
MEDIA_ROOT = ROOT("media_root")
STATIC_URL = "/static/"
STATIC_ROOT = ROOT("static_root")
STATICFILES_DIRS = (
    # CSS files
    ("css", ROOT("static/css")),
    # Fonts
    ("fonts", ROOT("static/fonts")),
    # JS files
    ("js", ROOT("static/js")),
    # Images
    ("img", ROOT("static/img")),
)


# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"
    },
]


def get_jinja2_env():
    from django_jinja.backend import Jinja2
    return Jinja2.get_default().env


COMPRESS_JINJA2_GET_ENVIRONMENT = get_jinja2_env


CKEDITOR_CONFIGS = {
    "default": {
        "language": "el",
        "defaultLanguage": "en",
        # "skin": "office2013",
        "entities_latin": False,
        "entities_greek": False,
        "toolbar_Basic": [["Source", "-", "Bold", "Italic"]],
        "toolbar_enhanced": [
            {
                "name": "clipboard",
                "items": [
                    "Cut",
                    "Copy",
                    "Paste",
                    "PasteText",
                    "PasteFromWord",
                    "-",
                    "Undo",
                    "Redo",
                ],
            },
            {
                "name": "editing",
                "items": ["Find", "-", "SelectAll"],
            },
            {
                "name": "basicstyles",
                "items": [
                    "Bold",
                    "Italic",
                    "Underline",
                    "Strike",
                    "Subscript",
                    "Superscript",
                    "-",
                    "RemoveFormat",
                ],
            },
            {
                "name": "paragraph",
                "items": [
                    "NumberedList",
                    "BulletedList",
                    "-",
                    "Outdent",
                    "Indent",
                    "-",
                    "Blockquote",
                    "-",
                    "JustifyLeft",
                    "JustifyCenter",
                    "JustifyRight",
                    "JustifyBlock",
                    "-",
                    "Language",
                ],
            },
            {"name": "links", "items": ["Link", "Unlink", "Anchor"]},
            {
                "name": "insert",
                "items": [
                    "Table",
                    "HorizontalRule",
                    "Smiley",
                    "SpecialChar",
                    "Maximize",
                ],
            },
            {
                "name": "styles",
                "items": ["Styles", "Format", "Font", "FontSize"],
            },
            {"name": "colors", "items": ["TextColor", "BGColor"]},
            # '/',  # put this to force next toolbar on new line
        ],
        "toolbar": "enhanced",  # put selected toolbar config here
        # 'height': 291,
        # 'width': '100%',
        "toolbarCanCollapse": True,
        "tabSpaces": 4,
        "removePlugins": "stylesheetparser,elementspath",
        "extraPlugins": ",".join(
            [
                # your extra plugins here
                "div",
                "autolink",
                "autoembed",
                "embedsemantic",
                "autogrow",
                "liststyle",
                "widget",
                "lineutils",
                "clipboard",
                "dialog",
                "dialogui",
            ]
        ),
    }
}


# https://docs.djangoproject.com/en/dev/ref/settings/#logging
# CRITICAL --> ERROR --> WARNING --> INFO --> DEBUG
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "production_only": {"()": "django.utils.log.RequireDebugFalse"},
        "development_only": {"()": "django.utils.log.RequireDebugTrue"},
    },
    "formatters": {
        "simple": {
            "format": "[%(asctime)s] %(levelname)s %(message)s",
            "datefmt": "%m/%d/%Y %H:%M:%S",
        },
        "verbose": {
            "format": "[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
            "datefmt": "%m/%d/%Y %H:%M:%S",
        },
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "filters": ["development_only"],
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        "mail_admins": {
            "level": "ERROR",
            "filters": ["production_only"],
            "class": "django.utils.log.AdminEmailHandler",
            "include_html": True,
        },
        "db_logfile": {
            "level": "WARNING",
            "filters": ["production_only"],
            "class": "logging.handlers.RotatingFileHandler",
            "filename": ROOT("logs/db.log"),
            "maxBytes": 1024 * 1024,
            "backupCount": 3,
            "formatter": "verbose",
        },
    },
    "loggers": {
        "django.db.backends": {
            "handlers": ["db_logfile"],
            "level": "WARNING",
            "propagate": False,
        },
        "django.request": {
            "handlers": ["mail_admins"],
            "level": "ERROR",
            "propagate": True,
        },
        # `email` is an explicit logger which will send email to ADMINS,
        # ONLY if the logger inside the module
        # (i.e email_logger = logging.getLogger('email')) will be called
        # with error (i.e email_logger.error(...))
        "email": {
            "handlers": ["mail_admins"],
            "level": "ERROR",
            "propagate": False,
        },
        # if a log of level INFO (or greater) occurred in the `cart` app,
        # log it in logs/my_apps.log
        # 'cart': {
        #    'handlers': ['my_apps'],
        #    'level': 'INFO',
        # },
        # if a log of level INFO (or greater) occurred in the `info` app,
        # log it in logs/my_apps.log & send emails
        # 'info': {
        #    'handlers': ['my_apps', 'mail_admins'],
        #    'level': 'INFO',
        #    'propagate': False
        # },
    },
}
