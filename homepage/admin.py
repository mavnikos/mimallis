from django.contrib import admin
from django.utils.html import mark_safe
from django.utils.translation import ugettext_lazy as _

from modeltranslation.admin import TranslationAdmin

from .models import (
    Slider,
    Introduction,
    Amenity,
    AmenityPhoto,
    HomepageSelect,
    HomepageTestimonial,
)


@admin.register(Slider)
class HomePagePhotosAdmin(TranslationAdmin):
    list_display = ("show_slider", "order", "active")
    list_editable = ("order",)
    readonly_fields = ("show_slider",)
    fields = (("order", "active"), ("show_slider", "image"), "title")

    def show_slider(self, obj):
        src = obj.image.url
        return mark_safe(f"<img src={src} width=100px>")

    show_slider.short_description = "Slider"


@admin.register(Introduction)
class IntroductionAdmin(TranslationAdmin):
    list_display = ("title",)
    fields = ("title", "text")

    def has_add_permission(self, request):
        return Introduction.objects.count() == 0


class AmenityPhotoInline(admin.StackedInline):
    model = AmenityPhoto
    fields = (("show_thumb_admin", "image", "order"),)
    readonly_fields = ("show_thumb_admin",)
    min_num = 1
    extra = 0

    def show_thumb_admin(self, obj):
        if obj.image:
            return mark_safe(f"<img src={obj.image.url} width=100px>")
        return mark_safe(
            _('<span style="color:red;font-weight:bold;">No image</span>')
        )

    show_thumb_admin.short_description = _("Preview")


@admin.register(Amenity)
class AmenityAdmin(TranslationAdmin):
    inlines = [AmenityPhotoInline]
    list_display = ("title",)
    fields = ("title", "text")

    def has_add_permission(self, request):
        return Amenity.objects.count() == 0


@admin.register(HomepageSelect)
class HomepageSelectAdmin(TranslationAdmin):
    list_display = ("title",)
    fields = ("title",)

    def has_add_permission(self, request):
        return HomepageSelect.objects.count() == 0


@admin.register(HomepageTestimonial)
class HomepageTestimonialAdmin(admin.ModelAdmin):
    list_display = ("visitor_name", "place", "order")
    list_editable = ("order",)
    fields = ("order", "visitor_name", "place", "review", "house")
