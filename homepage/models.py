from django.db import models
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField

from house.models import House


class Slider(models.Model):
    image = models.ImageField(
        _("Slider image"),
        upload_to="homepage",
        help_text=_(
            "Image slider on homepage.<br>Optimum dimensions: 1920x520px."
        ),
    )
    order = models.SmallIntegerField(
        _("Order of appearance"),
        blank=True,
        null=True,
        help_text=_("Number of appearance of this photo."),
    )
    active = models.BooleanField(default=True)
    title = models.CharField(max_length=40, blank=True, null=True)

    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    class Meta:
        verbose_name = _("slider image")
        verbose_name_plural = _("slider images")
        ordering = ["order"]


class Introduction(models.Model):
    title = models.CharField(max_length=40)
    text = RichTextField()

    class Meta:
        verbose_name = _("introduction")
        verbose_name_plural = _("introduction")

    def __str__(self):
        return self.title


class Amenity(models.Model):
    title = models.CharField(max_length=40)
    text = RichTextField()

    class Meta:
        verbose_name = _("amenity")
        verbose_name_plural = _("amenities")

    def __str__(self):
        return self.title


class AmenityPhoto(models.Model):
    amenity = models.ForeignKey(Amenity, models.CASCADE, related_name="images")
    image = models.ImageField(
        help_text=_(
            "Amenity photo on homepage.<br>Optimum dimensions: 450x500px."
        )
    )
    order = models.PositiveSmallIntegerField(blank=True, null=True)

    class Meta:
        verbose_name = _("amenity photo")
        verbose_name_plural = _("amenities photos")

    def __str__(self):
        return self.amenity.title


class HomepageSelect(models.Model):
    title = models.CharField(max_length=40)

    class Meta:
        verbose_name = _("select house")
        verbose_name_plural = _("select houses")

    def __str__(self):
        return self.title


class HomepageTestimonial(models.Model):
    visitor_name = models.CharField(_("Visitor name"), max_length=30)
    review = models.TextField(_("Visitor's review text"))
    place = models.CharField(_("Place & Country"), max_length=50)
    house = models.ForeignKey(
        House, on_delete=models.DO_NOTHING, verbose_name=_("House stayed")
    )
    order = models.SmallIntegerField(
        _("Order of appearance"),
        blank=True,
        null=True,
        help_text=_("Number of appearance of this testimonial."),
    )

    class Meta:
        verbose_name = _("Testimonial")
        verbose_name_plural = _("Testimonials")
        ordering = ["order"]

    def __str__(self):
        return self.visitor_name
