from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _

from .models import (
    Slider,
    Introduction,
    Amenity,
    HomepageSelect,
    HomepageTestimonial,
)


def get_client_ip(request):
    x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR", "")
    if x_forwarded_for:
        return x_forwarded_for.split(",")[0]
    return request.META.get("REMOTE_ADDR", "")


def index(request):
    sliders = Slider.objects.filter(active=True).all()
    introduction = Introduction.objects.first()
    amenity = Amenity.objects.first()
    homepage_select = HomepageSelect.objects.first()
    homepage_testimonials = HomepageTestimonial.objects.all()
    context = {
        "sliders": sliders,
        "introduction": introduction,
        "amenity": amenity,
        "homepage_select": homepage_select,
        "homepage_testimonials": homepage_testimonials,
        "client_ip": get_client_ip(request),
        "page_title": _("Homepage"),
        "meta_description": _(
            "Mimallis is a family-run business providing excellent "
            "hospitality. It's composed of 3 fully renovated houses. "
            "Two of them are in Plaka and the other in Klima."
        ),
    }
    return render(request, "homepage/index.html", context)
