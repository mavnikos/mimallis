from modeltranslation.translator import translator, TranslationOptions

from .models import Slider, Introduction, Amenity, HomepageSelect


class SliderTranslate(TranslationOptions):
    fields = ("title", )


class IntroductionTranslate(TranslationOptions):
    fields = ("title", )


class AmenityTranslate(TranslationOptions):
    fields = ("title", "text")


class HomepageSelectTranslate(TranslationOptions):
    fields = ("title", )


translator.register(Slider, SliderTranslate)
translator.register(Introduction, IntroductionTranslate)
translator.register(Amenity, AmenityTranslate)
translator.register(HomepageSelect, HomepageSelectTranslate)
