# Generated by Django 2.1.7 on 2019-02-22 12:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0006_auto_20190222_1326'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slider',
            name='image',
            field=models.ImageField(help_text='Image slider on homepage.<br>Optimum dimensions: 1920x1080px.', upload_to='homepage', verbose_name='Slider image'),
        ),
    ]
