from datetime import datetime

from django import forms
from django.forms.widgets import TextInput
from django.utils.translation import ugettext_lazy as _

from house.models import House

DATE_FORMAT = "%d %b %Y"


class TelInput(TextInput):
    input_type = "tel"


class ContactForm(forms.Form):
    name = forms.CharField(
        label=_("Name"),
        widget=forms.TextInput(
            attrs={
                "required": "True",
                "id": "name",
            }
        ),
    )

    from_email = forms.EmailField(
        label="Email",
        widget=forms.EmailInput(
            attrs={
                "required": "True",
                "id": "email",
            }
        ),
    )

    house = forms.ModelChoiceField(
        queryset=House.objects.all(),
        to_field_name="slug",
        initial=0,
        widget=forms.Select(attrs={"required": "True"}),
    )

    start_date = forms.CharField(
        label=_("Stay from"),
        widget=forms.TextInput(
            attrs={"required": "True", "class": "datepicker"}
        ),
    )

    until_date = forms.CharField(
        label=_("Stay until"),
        widget=forms.TextInput(
            attrs={"required": "True", "class": "datepicker"}
        ),
    )

    adults = forms.ChoiceField(
        label=_("Adults"),
        initial="2",
        choices=(("1", "1"), ("2", "2"), ("3", "3"), ("4", "4"))
    )

    kids = forms.ChoiceField(
        label=_("Kids"),
        initial="0",
        choices=(("0", "0"), ("1", "1"), ("2", "2"), ("3", "3")),
        required=False,
    )

    # kids_age = forms.CharField(
    #     label=_("Kids age"), required=False, widget=TelInput()
    # )

    message = forms.CharField(
        label=_("Message"),
        required=False,
        widget=forms.Textarea(
            attrs={
                "id": "message",
                "rows": 3,
            }
        ),
    )

    def clean_start_date(self):
        data = self.cleaned_data["start_date"]
        try:
            datetime.strptime(data, DATE_FORMAT)
        except ValueError:
            raise forms.ValidationError(_("Invalid start date!"))
        return data

    def clean_until_date(self):
        data = self.cleaned_data["until_date"]
        try:
            datetime.strptime(data, DATE_FORMAT)
        except ValueError:
            raise forms.ValidationError(_("Invalid until date!"))
        return data
