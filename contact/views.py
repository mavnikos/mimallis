from smtplib import SMTPException

from django.conf import settings
from django.contrib import messages
from django.urls import reverse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.core.mail import EmailMessage, BadHeaderError

from jinja2 import Environment, PackageLoader
from jinja2.exceptions import TemplateNotFound

from house.models import House
from .forms import ContactForm


def get_template(template_name="email_body.txt"):
    env = Environment(loader=PackageLoader(__package__, "templates"))
    template = None
    try:
        template = env.get_template(template_name)
    except TemplateNotFound:
        pass
    return template


def kids(data):
    total_kids = 0
    try:
        total_kids = int(data.get("kids", 0))
    except ValueError:
        pass
    kids_ages = {
        f"Kid {kid + 1} age": data.get(f"kid_age_{kid + 1}", "-")
        for kid in range(total_kids)
    }
    return kids_ages


def email_body(data):
    house_name = House.objects.get(slug=data.get("house")).name
    kids_ages = kids(data)
    template = get_template()
    context = {"data": data, "house_name": house_name, "kids_ages": kids_ages}
    body = template.render(**context)
    return body


def index(request):
    contact_form = ContactForm()
    if request.method == "POST":
        post_data = request.POST
        if post_data.get("send_email"):
            contact_form = ContactForm(post_data)
            if contact_form.is_valid():
                subject = f"Message from {post_data.get('name')}"
                body = email_body(post_data)
                try:
                    email = EmailMessage(
                        subject=subject,
                        body=body,
                        from_email=settings.DEFAULT_FROM_EMAIL,
                        to=[settings.DEFAULT_FROM_EMAIL],
                        reply_to=[post_data.get("from_email")],
                    )
                    email.send(fail_silently=True)
                except (BadHeaderError, SMTPException):
                    messages.error(
                        request,
                        _("Form error submission. Please try again!"),
                        extra_tags="form_error",
                    )
                else:
                    messages.success(
                        request,
                        _("Thank you for your message!"),
                        extra_tags="form_success",
                    )
                    return HttpResponseRedirect(reverse("contact"))
    page_title = _("Contact Us")
    meta_description = _(
        "Contact Mimallis houses. "
        "Tel: +30 22870 21094, Mob: +30 6972 808758, email: info@mimallis.gr. "
        "We provide quick response to your requests."
    )
    context = {
        "contact_form": contact_form,
        "page_title": page_title,
        "meta_description": meta_description,
    }

    return render(request, "contact.html", context)


# name = post_data.get("name")
# from_email = post_data.get("from_email")
# house = post_data.get("house")
# start_date = post_data.get("start_date")
# until_date = post_data.get("until_date")
# adults = post_data.get("adults")
# kids = post_data.get("kids")
# kids_age = post_data.get("kids_age")
# message = post_data.get("message")
# enhanced_message = _(
#     "User's name: {0}\n"
#     "User's email: {1}\n\n"
#     "{2}"
# ).format(name, from_email, message)
# body = _(
#     f"User's name: {name}\n"
#     f"User's email: {from_email}\n"
#     f"Check in: {start_date}\n"
#     f"Check out: {until_date}\n\n"
#     f"{message}"
# )
