from django.contrib import admin
from django.utils.html import mark_safe, format_html
from django.utils.translation import ugettext_lazy as _

from modeltranslation.admin import TranslationAdmin

from .models import House, HouseImageDeposit, HouseIndex, HouseAmenity


class HouseImageDepositInline(admin.StackedInline):
    model = HouseImageDeposit
    fields = (("show_thumb_admin", "large_img", "order"),)
    readonly_fields = ("show_thumb_admin",)
    min_num = 1
    extra = 0

    def show_thumb_admin(self, obj):
        if obj.large_img:
            src = obj.large_img.url
            return mark_safe(f"<img src={src} width=100px>")
        return mark_safe(
            _('<span style="color:red;font-weight:bold;">No image</span>')
        )

    show_thumb_admin.short_description = _("Preview")


@admin.register(HouseAmenity)
class HouseAmenityAdmin(TranslationAdmin):
    pass


@admin.register(House)
class HouseAdmin(TranslationAdmin):
    inlines = [HouseImageDepositInline]
    filter_horizontal = ("amenities",)
    list_display = ("name", "order")
    list_editable = ("order",)
    readonly_fields = ("show_thumbnail",)
    save_on_top = True

    fields = (
        "order",
        "name",
        ("show_thumbnail", "top_image"),
        "short_description",
        "description",
        "amenities",
        "meta_description",
    )

    def show_thumbnail(self, obj):
        src = obj.top_image.url
        return format_html(f"<img src={src} height=50px>")

    show_thumbnail.short_description = _("Top Image")


@admin.register(HouseIndex)
class HouseIndexAdmin(admin.ModelAdmin):
    list_display = ("show_thumbnail",)
    fields = (("show_thumbnail", "top_image"),)
    readonly_fields = ("show_thumbnail",)

    def show_thumbnail(self, obj):
        return format_html(f"<img src={obj.top_image.url} height=90px>")

    show_thumbnail.short_description = _("Top Image")

    def has_add_permission(self, request):
        return HouseIndex.objects.count() == 0
