from django.db import models
from django.urls import reverse_lazy
from django.utils.text import slugify
from django.utils.translation import override
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField


class HouseAmenity(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = _("amenity")
        verbose_name_plural = _("amenities")

    def __str__(self):
        return self.name


class House(models.Model):
    name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(unique=True)
    amenities = models.ManyToManyField(HouseAmenity, blank=True)
    short_description = models.TextField(
        help_text=_("Description that appears in the house index page.")
    )
    description = RichTextField(_("Description"), null=True, blank=True)
    top_image = models.ImageField(
        _("Top image"),
        upload_to="top_images",
        help_text=_(
            "Select image for the top of the page.<br>"
            "Max dimensions: <strong>1920x300px</strong>.<br>"
        ),
    )
    order = models.SmallIntegerField(
        _("Order of appearance"),
        blank=True,
        null=True,
        help_text=_("Number of appearance of this house."),
    )
    meta_description = models.TextField(max_length=160)
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    class Meta:
        verbose_name = _("house")
        verbose_name_plural = _("houses")
        ordering = ["order"]

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        with override(language="en"):
            self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse_lazy("house_detail", kwargs={"house_slug": self.slug})


class HouseImageDeposit(models.Model):
    house = models.ForeignKey(
        House,
        verbose_name=_("House"),
        related_name="images",
        on_delete=models.CASCADE,
    )
    large_img = models.ImageField(
        _("Large image"),
        upload_to="house",
        help_text=_(
            "Select house's large image.<br>"
            "Optimum dimensions: <strong>750x500px</strong>.<br>"
        ),
    )
    order = models.SmallIntegerField(
        _("Order of appearance"),
        blank=True,
        null=True,
        help_text=_("Number of appearance of this image."),
    )

    class Meta:
        verbose_name = _("house's large image")
        verbose_name_plural = _("house's large images")
        ordering = ["order", "house__name"]

    def __str__(self):
        return self.house.name


class HouseIndex(models.Model):
    top_image = models.ImageField(
        help_text=_(
            "Select image for the top of the page.<br>"
            "Max dimensions: <strong>1920x300px</strong>.<br>"
        )
    )

    class Meta:
        verbose_name = _("top image")
        verbose_name_plural = _("top image")
