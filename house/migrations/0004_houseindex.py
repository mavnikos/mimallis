# Generated by Django 2.1.7 on 2019-02-22 12:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('house', '0003_auto_20190222_1132'),
    ]

    operations = [
        migrations.CreateModel(
            name='HouseIndex',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('top_image', models.ImageField(upload_to='')),
            ],
            options={
                'verbose_name': 'top image',
                'verbose_name_plural': 'top image',
            },
        ),
    ]
