from modeltranslation.translator import translator, TranslationOptions

from .models import House, HouseAmenity


class HouseAmenityTranslate(TranslationOptions):
    fields = ("name",)


class HouseTranslate(TranslationOptions):
    fields = (
        "name",
        "short_description",
        "description",
        "meta_description",
    )


translator.register(House, HouseTranslate)
translator.register(HouseAmenity, HouseAmenityTranslate)
