from django.shortcuts import render, get_object_or_404
from django.utils.translation import ugettext_lazy as _

from .models import House, HouseIndex


def index(request):
    houses = House.objects.all()
    house_index = HouseIndex.objects.first()
    page_title = _("Houses")
    meta_description = _(
        "List of the 3 available traditional houses. Two of them are in the "
        "picturesque village of Plaka and the other one in the idyllic "
        "fishermen's village of Klima."
    )
    context = {
        "houses": houses,
        "house_index": house_index,
        "page_title": page_title,
        "meta_description": meta_description,
    }
    return render(request, "house/index.html", context)


def detail(request, house_slug):
    house = get_object_or_404(House, slug=house_slug)
    context = {
        "house": house,
        "page_title": house.name,
    }
    return render(request, "house/detail.html", context)
