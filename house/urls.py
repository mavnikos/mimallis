from django.urls import path

from .views import index, detail


urlpatterns = [
    path("", index, name="house_list"),
    path("<slug:house_slug>/", detail, name="house_detail"),
]
